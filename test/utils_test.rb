require 'test_helper'

class UtilsTest < ActiveSupport::TestCase
  test "get_foreign_keys" do
    fks = ForeignKeyChecker::Utils.get_foreign_keys
    assert_equal 3, fks.size
  end
end
