require 'test_helper'

class ForeignKeyChecker::Test < ActiveSupport::TestCase
  self.use_transactional_tests = false
  test "migration" do
    block = proc do |nullable|
      SelfReferencer.create!(parent_id: -1)
      assert_equal 3, ForeignKeyChecker.check[:foreign_keys].size
      ForeignKeyChecker.check[:foreign_keys].each do |result|
        [
          (nullable && result.nullable?) ? result.to_zombie.migration(set_null: true) : nil,
          result.to_zombie.migration,
          result.migration
        ].compact.each do |migration|
          migration.split("\n").each do |line|
            eval "ActiveRecord::Migration.#{line.strip}" if !line.blank?
          end
        end
      end
      assert_empty ForeignKeyChecker.check[:foreign_keys]
    end
    [true, false].each do |nullable|
      if ApplicationRecord.connection_db_config.configuration_hash[:adapter] == 'mysql2'
        io = StringIO.new
        ActiveRecord::SchemaDumper.dump ActiveRecord::Base.connection, io
        error_in_block = true
        begin
          block.call(nullable)
          error_in_block = false
          ApplicationRecord.connection.execute('SET FOREIGN_KEY_CHECKS=0;')
          eval(io.string)
          ApplicationRecord.connection.execute('SET FOREIGN_KEY_CHECKS=1;')
        rescue => e
          if error_in_block
            ApplicationRecord.connection.execute('SET FOREIGN_KEY_CHECKS=0;')
            eval(io.string)
            ApplicationRecord.connection.execute('SET FOREIGN_KEY_CHECKS=1;')
          end
          raise e
        end
      else
        ApplicationRecord.transaction do
          block.call(nullable)
          raise ActiveRecord::Rollback
        end
      end
    end
  end
end
