# == Schema Information
#
# Table name: geography_streets
#
#  id         :bigint           not null, primary key
#  name       :string
#  city_id    :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Geography::Street < ApplicationRecord
  belongs_to :city
end
