# == Schema Information
#
# Table name: self_referencers
#
#  id             :bigint           not null, primary key
#  parent_id      :bigint
#  good_parent_id :bigint
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_self_referencers_on_good_parent_id  (good_parent_id)
#  index_self_referencers_on_parent_id       (parent_id)
#
# Foreign Keys
#
#  fk_rails_...  (good_parent_id => self_referencers.id)
#
class SelfReferencer < ApplicationRecord
  belongs_to :parent, class_name: 'SelfReferencer', required: false
  belongs_to :good_parent, class_name: 'SelfReferencer', required: false
  has_many :children, class_name: 'SelfReferencer', dependent: :destroy, foreign_key: :parent_id
  has_many :good_children, class_name: 'SelfReferencer', dependent: :destroy, foreign_key: :good_parent_id
end
