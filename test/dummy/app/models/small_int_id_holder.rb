# == Schema Information
#
# Table name: small_int_id_holders
#
#  pk_name    :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class SmallIntIdHolder < ApplicationRecord
  has_many :referers, class_name: 'SmallIntIdReferer', foreign_key: :holder_id
end
