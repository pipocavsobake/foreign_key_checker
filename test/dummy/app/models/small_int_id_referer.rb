# == Schema Information
#
# Table name: small_int_id_referers
#
#  id         :bigint           not null, primary key
#  holder_id  :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_small_int_id_referers_on_holder_id  (holder_id)
#
class SmallIntIdReferer < ApplicationRecord
  belongs_to :holder, class_name: 'SmallIntIdHolder'
end
