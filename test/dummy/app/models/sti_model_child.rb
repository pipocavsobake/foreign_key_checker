# == Schema Information
#
# Table name: sti_models
#
#  id         :bigint           not null, primary key
#  type       :string
#  user_id    :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_sti_models_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class StiModelChild < StiModel

end
