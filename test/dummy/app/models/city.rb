# == Schema Information
#
# Table name: cities
#
#  id         :bigint           not null, primary key
#  country_id :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class City < ApplicationRecord
  has_many :users
  belongs_to :country
  has_many :streets, dependent: :destroy
  has_one :main_street
end
