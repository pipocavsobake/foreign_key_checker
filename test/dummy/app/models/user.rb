# == Schema Information
#
# Table name: users
#
#  id         :bigint           not null, primary key
#  city_id    :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_users_on_city_id  (city_id)
#
class User < ApplicationRecord
  belongs_to :city, required: false
  has_and_belongs_to_many :cities
  has_many :sti_models, dependent: :destroy
end
