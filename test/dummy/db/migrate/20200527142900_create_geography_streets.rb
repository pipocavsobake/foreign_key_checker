class CreateGeographyStreets < ActiveRecord::Migration[6.0]
  def change
    create_table :geography_streets do |t|
      t.string :name
      t.bigint :city_id

      t.timestamps
    end
  end
end
