class CreateSelfReferencers < ActiveRecord::Migration[6.0]
  def change
    create_table :self_referencers do |t|
      t.references :parent, foreign_key: false
      t.references :good_parent, foreign_key: { to_table: :self_referencers }

      t.timestamps
    end
  end
end
