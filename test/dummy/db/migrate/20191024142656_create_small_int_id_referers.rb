class CreateSmallIntIdReferers < ActiveRecord::Migration[6.0]
  def change
    create_table :small_int_id_referers do |t|
      t.references :holder, null: false, foreign_key: false

      t.timestamps
    end
  end
end
