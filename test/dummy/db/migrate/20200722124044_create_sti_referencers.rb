class CreateStiReferencers < ActiveRecord::Migration[6.0]
  def change
    create_table :sti_referencers do |t|
      t.references :sti_model, foreign_key: true

      t.timestamps
    end
  end
end
