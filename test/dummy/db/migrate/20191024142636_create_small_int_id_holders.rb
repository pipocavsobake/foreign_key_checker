class CreateSmallIntIdHolders < ActiveRecord::Migration[6.0]
  def change
    create_table :small_int_id_holders, id: :integer, primary_key: :pk_name do |t|

      t.timestamps
    end
  end
end
