class CreateStiModels < ActiveRecord::Migration[6.0]
  def change
    create_table :sti_models do |t|
      t.string :type
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
