class CreateTableWithoutModel < ActiveRecord::Migration[6.0]
  def change
    create_table :table_without_models do |t|
      t.string :name
    end
  end
end
