# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_22_125730) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", force: :cascade do |t|
    t.bigint "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cities_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "city_id", null: false
  end

  create_table "geography_streets", force: :cascade do |t|
    t.string "name"
    t.bigint "city_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "self_referencers", force: :cascade do |t|
    t.bigint "parent_id"
    t.bigint "good_parent_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["good_parent_id"], name: "index_self_referencers_on_good_parent_id"
    t.index ["parent_id"], name: "index_self_referencers_on_parent_id"
  end

  create_table "small_int_id_holders", primary_key: "pk_name", id: :serial, force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "small_int_id_referers", force: :cascade do |t|
    t.bigint "holder_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["holder_id"], name: "index_small_int_id_referers_on_holder_id"
  end

  create_table "sti_models", force: :cascade do |t|
    t.string "type"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_sti_models_on_user_id"
  end

  create_table "sti_referencers", force: :cascade do |t|
    t.bigint "sti_model_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["sti_model_id"], name: "index_sti_referencers_on_sti_model_id"
  end

  create_table "table_without_models", force: :cascade do |t|
    t.string "name"
  end

  create_table "users", force: :cascade do |t|
    t.bigint "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_users_on_city_id"
  end

  add_foreign_key "self_referencers", "self_referencers", column: "good_parent_id"
  add_foreign_key "sti_models", "users"
  add_foreign_key "sti_referencers", "sti_models"
end
