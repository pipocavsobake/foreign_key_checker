require 'test_helper'

class CheckersTablesTest < ActiveSupport::TestCase
  test 'without_models' do
    bad_tables = ForeignKeyChecker::Checkers::Tables.without_models
    assert_equal(['table_without_models'], bad_tables)
  end
end
