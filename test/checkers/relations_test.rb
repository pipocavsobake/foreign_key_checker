require 'test_helper'

class CheckersRelationsTest < ActiveSupport::TestCase
  test 'check_by_join' do
    results = []
    ForeignKeyChecker::Checkers::Relations.check_by_join do |result|
      results << result
    end
    assert_equal(19, results.size)
    results.reject!(&:ok)
    assert(results.find { |r| r.model == City && r.association.name == :country && r.association.is_a?(ActiveRecord::Reflection::BelongsToReflection) })
    assert(results.find { |r| r.model == City && r.association.name == :streets && r.association.is_a?(ActiveRecord::Reflection::HasManyReflection) })
    assert(results.find { |r| r.model == City && r.association.name == :main_street && r.association.is_a?(ActiveRecord::Reflection::HasOneReflection) })
    assert_equal(3, results.size)
  end
end
