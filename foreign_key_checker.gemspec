$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "foreign_key_checker/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "foreign_key_checker"
  spec.version     = ForeignKeyChecker::VERSION
  spec.authors     = ["AnatolyShirykalov"]
  spec.email       = ["pipocavsobake@gmail.com"]
  spec.homepage    = "https://gitlab.com/pipocavsobake/foreign_key_checker"
  spec.summary     = "Find problems with relations in active_record models"
  spec.description = "Run task to obtain problems with your database"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", '>=6.1.0'

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "pg"
  spec.add_development_dependency "mysql2"

  spec.add_development_dependency "tiny_tds"
  #spec.add_development_dependency "activerecord-sqlserver-adapter"

  spec.add_development_dependency "irb"
  spec.add_development_dependency "e2mmap"
  spec.add_development_dependency "annotate"

end
