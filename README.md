# ForeignKeyChecker
This gem checks `belongs_to` ActiveRecord relations. It finds
1. Nonpolymorphic relations without a `foreign_key`
2. records without a record in related table (example: `city_id` is 1000, but there is not city with id 1000)
3. broken relations: if you try to join such relation (example: `City.joins(:country)`), there is an exception.

## Usage
Generate migration to fix foreign key problems
```bash
bundle exec rails generate foreign_key_checker:migration
```


Print information about foreign key problems
```bash
bundle exec rake foreign_key_check
```

Or use it inside your application
```ruby
ForeignKeyChecker.check.each do |key, result|
  ...
end
```

Get general information about foreign keys
```ruby
ForeignKeyChecker::Utils.get_foreign_keys_hash
# =>  {"users"=>[#<ForeignKeyChecker::Utils::Result:0x00005645e51756e8 @from_table="user_rating_changes", @from_column="user_id", @to_table="users", @to_column="id">]}
```

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'foreign_key_checker'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install foreign_key_checker
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
