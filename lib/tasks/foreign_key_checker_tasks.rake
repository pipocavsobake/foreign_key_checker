desc "Explaining what the task does"
task foreign_key_check: :environment do
  ForeignKeyChecker.check.each do |key, results|
    puts key if results.any?
    results.each do |result|
      puts result.message
    end
  end
end
