require 'foreign_key_checker/utils'

# Стоит использовать в тех случаях, когда подключается rails к уже существующей базе
# Можно проверить, для каких таблиц всё ещё нет ActiveRecord-моделей
module ForeignKeyChecker::Checkers::Tables
  SPECIAL_TABLES = ['schema_migrations', 'ar_internal_metadata'].freeze
  # Список таблиц, не описанных моделями (в том числе HABTM-моделями, сгенерированными автоматически rails)
  def self.without_models(specification_name = 'primary')
    Rails.application.eager_load!

    actual_tables = ForeignKeyChecker::Utils.get_tables
    actual_tables - modelised_tables(specification_name) - SPECIAL_TABLES
  end

  def self.without_foreign_keys(specification_name = 'primary')
    all_fks = ForeignKeyChecker::Utils.get_foreign_keys
    results = []
    models(specification_name).each do |model|
      model.column_names.each do |column_name|
        next unless column_name.ends_with?('_id')
        next if all_fks.find { |fk| fk.from_table = model.table_name && fk.from_column == column_name }

        table_name = column_name.delete_suffix('_id')
        results << ["#{table_name}.#{column_name}"]
      end
    end
  end

  def self.modelised_tables(specification_name = 'primary')
    models(specification_name).map(&:table_name)
  end

  def self.models(specification_name = 'primary')
    ActiveRecord::Base.descendants.select do |model|
      model.connection_specification_name == specification_name
    end
  end


  def self.common_tables
    ForeignKeyChecker::Utils.get_tables - SPECIAL_TABLES
  end

  class Result
    attr_reader :table_name, :foreign_keys, :internal_references
    def initialize(**args)
      %i[table_name foreign_keys internal_references].each do |key|
        instance_variable_set("@#{key}", args[key] || args[key].to_s)
      end
    end

    def ok?
      foreign_keys.blank?
    end

    def referenced?
      foreign_keys.present?
    end

    def ext_ref?
      !internal_references
    end

  end

  def self.check
    tables = without_models
    fks = ForeignKeyChecker::Utils.get_foreign_keys_hash
    fks.default = []
    tables.map do |table|
      Result.new(
        table_name: table,
        foreign_keys: fks[table] || [],
        internal_references: (fks[table].map(&:from_table) - tables).empty?,
      )
    end
  end

  # TODO вспомнить, что я тут задумал
  def self.ordered(results = check)
    return results if results.size == 0
    oks = results.select(&:ok?)
    if oks.size == 0
      raise "no ok"
    end
  end

end
