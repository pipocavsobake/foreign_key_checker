module ForeignKeyChecker
  class Railtie < ::Rails::Railtie
    rake_tasks do
      load 'tasks/foreign_key_checker_tasks.rake'
    end
    generators do
      require 'generators/foreign_key_checker/migration/migration_generator.rb'
      require 'generators/foreign_key_checker/models_generator.rb'
    end
  end
end
