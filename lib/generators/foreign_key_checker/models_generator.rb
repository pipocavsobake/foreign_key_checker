require 'rails/generators/active_record'
module ForeignKeyChecker
  module Generators
    class ModelsGenerator < ActiveRecord::Generators::Base
      desc "generates models for all tables in development database"
      argument :name, type: :string, default: 'FixForeignKeys'

      source_root File.expand_path('templates', __dir__)

      def install
        ForeignKeyChecker::Utils::BelongsTo.build_classes(ActiveRecord::Base.connection).each do |object|
          file_path = "app/models/#{object[:class_name].underscore}.rb"
          @object = object
          template 'models/model.rb.erb', file_path
        end
      end


    end
  end
end
